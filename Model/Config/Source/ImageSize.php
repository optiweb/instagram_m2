<?php
/**
 * @author Urban Kovač
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 * @package Optiweb_Instagram
 */

namespace Optiweb\Instagram\Model\Config\Source;

class ImageSize implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'thumbnail', 'label' => 'Thumbnail (150x150px)'],
            ['value' => 'low_resolution', 'label' => 'Small (306x306px)'],
            ['value' => 'standard_resolution', 'label' => 'Big (612x612px)'],
        ];
    }
}
