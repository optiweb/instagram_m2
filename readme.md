#Magento 2 - Instagram
This extension will show Instagram feeds from your account.

___

##Features:
- show instagram feeds from your account
- set how many feeds will be shown
- set image size
- uses magento cache 

##Compatibility:
Tested in Magento 2.3.3. It should work with all Magento 2 installations

##Known issues:
- there are currently no known issues

##Installation:
1. install module:
    1. `composer require optiweb/module-instagram` OR
    1. download a zip and upload contents to `app/code/Optiweb/Instagram` folder
2. enable module: `php bin/magento module:enable Optiweb_Instagram`
2. run `php bin/magento setup:upgrade` from the root directory of your Magento installation
3. go to `Content > Widget` and create new widget
4. select type "Instagram Widget"

##Licence:
MIT. (see LICENCE.txt)

Version: 1.1.10