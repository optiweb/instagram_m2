<?php
/**
 * @author Urban Kovač
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 * @package Optiweb_Instagram
 */

namespace Optiweb\Instagram\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Optiweb\Instagram\Helper\Cache;

class Instagram extends Template implements BlockInterface
{
    
    /**
     * @var string
     */
    protected $_template = "widget/instagram.phtml";
    
    /**
     * @var
     */
    protected $_httpClientFactory;
    
    /**
     * @var
     */
    protected $_cacheHelper;
    
    /**
     * Instagram constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        ZendClientFactory $httpClientFactory,
        Cache $cacheHelper,
        Template\Context $context,
        array $data = []
    ) {
        $this->_httpClientFactory = $httpClientFactory;
        $this->_cacheHelper = $cacheHelper;
        parent::__construct($context, $data);
    }
    

    /**
     * @return post object
     */
    public function getInstagramContent()
    {
        $cacheId = $this->_cacheHelper->getId("instafeed");
        if($cache = $this->_cacheHelper->load($cacheId)){
            if($cache === "ERROR"){
                return null;
            }
            return json_decode($cache, true);
        }
        
        $userID = $this->getData('userId');
        $accessToken = $this->getData('accessToken');
    
        if(empty($userID) || empty($accessToken)){
            return null;
        }
        
        $count = intval($this->getData('count'));
        if(!isset($count) || empty($count) || ($count < 1)){
            $count = 1;
        }
        elseif ($count > 20){
            $count = 20;
        }
        
        $url = "https://api.instagram.com/v1/users/". $userID ."/media/recent";
        
        $client = $this->_httpClientFactory->create();
        $client->setUri($url);
        $client->setParameterGet("access_token", $accessToken);
        $client->setParameterGet("count", $count);
        $request = $client->request(\Zend_Http_Client::GET);
        $status = $request->getStatus();
        $response = $request->getBody();
        
        if($status == 200){
            $response = json_decode($response, true);
            $this->_cacheHelper->save(json_encode($response['data']), $cacheId);
            return $response['data'];
        }
        else {
            $this->_cacheHelper->save("ERROR", $cacheId, 3600);
        }
        
        return null;
    }
    

    /**
     * @return post object
     */
    public function getInstagramProfile()
    {
        $cacheId = $this->_cacheHelper->getId("profile");
        if($cache = $this->_cacheHelper->load($cacheId)){
            if($cache === "ERROR"){
                return null;
            }
            return json_decode($cache, true);
        }
        
        $userID = $this->getData('userId');
        $accessToken = $this->getData('accessToken');
    
        if(empty($userID) || empty($accessToken)){
            return null;
        }
        
        $url = "https://api.instagram.com/v1/users/self";
        
        $client = $this->_httpClientFactory->create();
        $client->setUri($url);
        $client->setParameterGet("access_token", $accessToken);
        $request = $client->request(\Zend_Http_Client::GET);
        $status = $request->getStatus();
        $response = $request->getBody();
        
        if($status == 200){
            $response = json_decode($response, true);
            $this->_cacheHelper->save(json_encode($response['data']), $cacheId);
            return $response['data'];
        }
        else {
            $this->_cacheHelper->save("ERROR", $cacheId, 3600);
        }
        
        return null;
    }


}
